package com.example.sharedpreferences.test;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.example.sharedpreferences.model.product.ProductDetails;

public class ProductDataSourceFactory extends DataSource.Factory {
    private MutableLiveData<PageKeyedDataSource<Integer, ProductDetails>> productLiveDataSource = new MutableLiveData<>();

    @Override
    public DataSource create() {
        ProductDataSource productDataSource = new ProductDataSource();
        productLiveDataSource.postValue(productDataSource);

        return productDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, ProductDetails>> getProductLiveDataSource() {
        return productLiveDataSource;
    }
}
