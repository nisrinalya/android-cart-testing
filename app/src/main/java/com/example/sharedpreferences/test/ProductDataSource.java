package com.example.sharedpreferences.test;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.example.sharedpreferences.model.AllProduct;
import com.example.sharedpreferences.model.product.ProductDetails;
import com.example.sharedpreferences.rest.ApiClient;
import com.example.sharedpreferences.rest.ApiInterface;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDataSource extends PageKeyedDataSource<Integer, ProductDetails> {
    public static final int PAGE_SIZE = 10;
    public static final int START = 0;

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull LoadInitialCallback<Integer, ProductDetails> callback) {
        ApiInterface apiInterface = ApiClient.getApiInterface();

        String from = Integer.toString(START);
        String to = Integer.toString(PAGE_SIZE);

        RequestBody rbFrom = RequestBody.create(from, MediaType.parse("text/plain"));
        RequestBody rbTo = RequestBody.create(to, MediaType.parse("text/plain"));

        apiInterface.getAllProductsPage(rbFrom, rbTo).enqueue(new Callback<AllProduct>() {
            @Override
            public void onResponse(Call<AllProduct> call, Response<AllProduct> response) {
                if (response.isSuccessful()) {
                    callback.onResult(response.body().getData(), null, START + PAGE_SIZE);
                }
            }

            @Override
            public void onFailure(Call<AllProduct> call, Throwable t) {

            }
        });
    }

    @Override
    public void loadBefore(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, ProductDetails> callback) {
        ApiInterface apiInterface = ApiClient.getApiInterface();

        String from = Integer.toString(params.key);
        String to = Integer.toString(PAGE_SIZE);

        RequestBody rbFrom = RequestBody.create(from, MediaType.parse("text/plain"));
        RequestBody rbTo = RequestBody.create(to, MediaType.parse("text/plain"));

        apiInterface.getAllProductsPage(rbFrom, rbTo).enqueue(new Callback<AllProduct>() {
            @Override
            public void onResponse(Call<AllProduct> call, Response<AllProduct> response) {
                Integer adjKey = (params.key > 0) ? params.key - PAGE_SIZE : null;
                if (response.isSuccessful()) {
                    callback.onResult(response.body().getData(), adjKey);
                }
            }

            @Override
            public void onFailure(Call<AllProduct> call, Throwable t) {

            }
        });
    }

    @Override
    public void loadAfter(@NonNull LoadParams<Integer> params, @NonNull LoadCallback<Integer, ProductDetails> callback) {
        ApiInterface apiInterface = ApiClient.getApiInterface();

        String from = Integer.toString(params.key);
        String to = Integer.toString(PAGE_SIZE);

        RequestBody rbFrom = RequestBody.create(from, MediaType.parse("text/plain"));
        RequestBody rbTo = RequestBody.create(to, MediaType.parse("text/plain"));

        apiInterface.getAllProductsPage(rbFrom, rbTo).enqueue(new Callback<AllProduct>() {
            @Override
            public void onResponse(Call<AllProduct> call, Response<AllProduct> response) {
                if (response.isSuccessful()) {
                    Integer key = params.key + PAGE_SIZE;
                    callback.onResult(response.body().getData(), key);
                }
            }

            @Override
            public void onFailure(Call<AllProduct> call, Throwable t) {

            }
        });
    }
}
