package com.example.sharedpreferences.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.sharedpreferences.model.AllProduct;
import com.example.sharedpreferences.model.product.ProductDetails;
import com.example.sharedpreferences.rest.ApiClient;
import com.example.sharedpreferences.rest.ApiInterface;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductDetailsViewModel extends ViewModel {
    private ApiInterface apiInterface;
    private MutableLiveData<List<ProductDetails>> productDetails;

    public LiveData<List<ProductDetails>> getProductDetails(RequestBody productId) {
        if (productDetails == null) {
            productDetails = new MutableLiveData<>();
            loadProductDetails(productId);
        }

        return productDetails;
    }

    private void loadProductDetails(RequestBody productId) {
        apiInterface = ApiClient.getApiInterface();
        apiInterface.getProductByID(productId).enqueue(new Callback<AllProduct>() {
            @Override
            public void onResponse(Call<AllProduct> call, Response<AllProduct> response) {
                productDetails.setValue(response.body().getData());
            }

            @Override
            public void onFailure(Call<AllProduct> call, Throwable t) {

            }
        });
    }
}
