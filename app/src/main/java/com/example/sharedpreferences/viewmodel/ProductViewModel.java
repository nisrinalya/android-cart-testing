package com.example.sharedpreferences.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.example.sharedpreferences.model.product.ProductDetails;
import com.example.sharedpreferences.test.ProductDataSource;
import com.example.sharedpreferences.test.ProductDataSourceFactory;

public class ProductViewModel extends ViewModel {
    public LiveData<PagedList<ProductDetails>> productPagedList;
    public LiveData<PageKeyedDataSource<Integer, ProductDetails>> liveDataSource;

    public ProductViewModel() {
        ProductDataSourceFactory productDataSourceFactory = new ProductDataSourceFactory();
        liveDataSource = productDataSourceFactory.getProductLiveDataSource();

        PagedList.Config pagedListConfig = (new PagedList.Config.Builder())
                .setEnablePlaceholders(false)
                .setPageSize(ProductDataSource.PAGE_SIZE).build();

        productPagedList = (new LivePagedListBuilder(productDataSourceFactory, pagedListConfig)).build();
    }
}
