package com.example.sharedpreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.sharedpreferences.adapter.MainAdapter;
import com.example.sharedpreferences.model.AllProduct;
import com.example.sharedpreferences.model.product.ProductDetails;
import com.example.sharedpreferences.rest.ApiClient;
import com.example.sharedpreferences.rest.ApiInterface;
import com.example.sharedpreferences.test.ProductAdapter;
import com.example.sharedpreferences.viewmodel.ProductViewModel;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private RecyclerView recycler;
    private MainAdapter adapter;
    private ProgressBar progressBar;

    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadWhiteStatusBar();

        apiInterface = ApiClient.getApiInterface();

        recycler = findViewById(R.id.main_content);
        progressBar = findViewById(R.id.main_progress);

        recycler.setLayoutManager(new GridLayoutManager(this, 2));
        recycler.setHasFixedSize(true);

        ProductViewModel pvm = ViewModelProviders.of(this).get(ProductViewModel.class);
        final ProductAdapter productAdapter = new ProductAdapter(this);

        pvm.productPagedList.observe(this, new Observer<PagedList<ProductDetails>>() {
            @Override
            public void onChanged(PagedList<ProductDetails> productDetails) {
                productAdapter.submitList(productDetails);
            }
        });

        recycler.setAdapter(productAdapter);

//        loadAll();
    }

    private void loadAll() {
        progressBar.setVisibility(View.VISIBLE);

        apiInterface.getAllProducts().enqueue(new Callback<AllProduct>() {
            @Override
            public void onResponse(Call<AllProduct> call, Response<AllProduct> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful()) {
                    List<ProductDetails> productList = response.body().getData();

                    RecyclerView.LayoutManager layoutManager = new GridLayoutManager(MainActivity.this, 2);
                    adapter = new MainAdapter(MainActivity.this, productList);

                    recycler.setLayoutManager(layoutManager);
                    recycler.setAdapter(adapter);
                } else {
                    try {
                        JSONObject object = new JSONObject(response.errorBody().string());
                        Toast.makeText(MainActivity.this, object.getString("message"), Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<AllProduct> call, Throwable t) {
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void loadWhiteStatusBar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    private void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();

        if (on) {
            params.flags |= bits;
        } else {
            params.flags &= ~bits;
        }

        window.setAttributes(params);
    }
}
