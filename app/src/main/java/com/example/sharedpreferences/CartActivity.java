package com.example.sharedpreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.sharedpreferences.adapter.CartAdapter;
import com.example.sharedpreferences.cart.CartManager;
import com.example.sharedpreferences.cart.ProductCart;

import java.util.List;
import java.util.Locale;

public class CartActivity extends AppCompatActivity {
    private CartManager cartManager;
    private List<ProductCart> cartList;
    private CartAdapter cartAdapter;

    RecyclerView recycler;
    TextView txtSubtotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        loadWhiteStatusBar();

        cartManager = new CartManager();
        cartList = cartManager.getCart(CartActivity.this);

        recycler = findViewById(R.id.cart_content);
        txtSubtotal = findViewById(R.id.cart_subtotal);

        if (cartList != null && cartList.size() != 0) {
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(CartActivity.this);
            cartAdapter = new CartAdapter(CartActivity.this, cartList, cartManager);

            recycler.setLayoutManager(layoutManager);
            recycler.setAdapter(cartAdapter);
        } else {
            recycler.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (cartAdapter != null && cartAdapter.getItemCount() != 0) {
            cartAdapter.notifyDataSetChanged();
        }

        if (cartList != null && cartList.size() != 0) {
            int subtotal = 0;
            for (ProductCart pc : cartList) {
                int qty = pc.getQty();
                int price = Integer.parseInt(pc.getProduct().getHarga());

                subtotal += price * qty;
            }

            txtSubtotal.setText("Rp " + String.format(Locale.getDefault(), "%,d", subtotal));
        } else {
            txtSubtotal.setText("Rp 0");
        }
    }

    private void loadWhiteStatusBar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    private void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();

        if (on) {
            params.flags |= bits;
        } else {
            params.flags &= ~bits;
        }

        window.setAttributes(params);
    }
}
