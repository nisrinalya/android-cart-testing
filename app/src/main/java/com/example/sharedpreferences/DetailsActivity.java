package com.example.sharedpreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharedpreferences.cart.CartManager;
import com.example.sharedpreferences.cart.store.CartStoreManager;
import com.example.sharedpreferences.model.product.ProductDetails;
import com.example.sharedpreferences.viewmodel.ProductDetailsViewModel;
import com.example.sharedpreferences.viewmodel.ProductViewModel;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;

public class DetailsActivity extends AppCompatActivity {
    ImageView image, btnPlus, btnMinus, btnAdd, btnCart;
    TextView name, price, total;

    int number;

    private CartManager cartManager;
    private CartStoreManager storeManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        loadTransparentStatusBar();

        cartManager = new CartManager();
        storeManager = new CartStoreManager();

        image = findViewById(R.id.details_image);
        btnPlus = findViewById(R.id.details_atc_plus);
        btnMinus = findViewById(R.id.details_atc_minus);
        btnAdd = findViewById(R.id.details_atc_button);
        btnCart = findViewById(R.id.details_atc_cart);

        name = findViewById(R.id.details_name);
        price = findViewById(R.id.details_price);
        total = findViewById(R.id.details_atc_total);

        Bundle extras = getIntent().getExtras();

        ProductDetails details = (ProductDetails) extras.getSerializable("prod_list");
        RequestBody rbProductId = RequestBody.create(details.getId(), MediaType.parse("text/plain"));
        ProductDetailsViewModel pdvm = ViewModelProviders.of(this).get(ProductDetailsViewModel.class);
        pdvm.getProductDetails(rbProductId).observe(this, new Observer<List<ProductDetails>>() {
            @Override
            public void onChanged(List<ProductDetails> productDetails) {
                for (ProductDetails pd : productDetails) {
                    int intPrice = Integer.parseInt(pd.getHarga());

                    if (pd.getFotoUtama() != null && pd.getFotoUtama().length() != 0) {
                        Picasso.get().load(pd.getFotoUtama()).into(image);
                    } else {
                        image.setImageResource(R.drawable.bakoelasa_imagedefault_01);
                    }

                    name.setText(pd.getNama());
                    price.setText("IDR" + String.format(Locale.getDefault(), "%,d", intPrice));
                    total.setText("0");

                    btnCart.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent cart = new Intent(DetailsActivity.this, CartStoreActivity.class);
                            startActivity(cart);
                        }
                    });

                    btnAdd.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int stock = Integer.parseInt(pd.getJumlahStok());
                            int qty = Integer.parseInt(total.getText().toString());

                            try {
                                if (stock == 0) {
                                    Toast.makeText(DetailsActivity.this, "No stock", Toast.LENGTH_SHORT).show();
                                } else {
                                    if (qty == 0) {
                                        Toast.makeText(DetailsActivity.this, "Please enter quantity", Toast.LENGTH_SHORT).show();
                                    } else if (qty > stock) {
                                        Toast.makeText(DetailsActivity.this, "Not enough stock", Toast.LENGTH_SHORT).show();
                                    } else {
                                        storeManager.addToCart(DetailsActivity.this, pd.getId(), pd.getStoreId(), pd.getStoreNama(), pd, qty);
                                    }
                                }
                            } catch (Exception e) {
                                Toast.makeText(DetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            }
        });

//        int intPrice = Integer.parseInt(details.getHarga());
//
//        if (details.getFotoUtama() != null && details.getFotoUtama().length() != 0) {
//            Picasso.get().load(details.getFotoUtama()).into(image);
//        } else {
//            image.setImageResource(R.drawable.bakoelasa_imagedefault_01);
//        }
//
//        name.setText(details.getNama());
//        price.setText("Rp " + String.format(Locale.getDefault(), "%,d", intPrice));
//        total.setText("0");
//
//        btnCart.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent cart = new Intent(DetailsActivity.this, CartStoreActivity.class);
//                startActivity(cart);
//            }
//        });
//
//        btnAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                int stock = Integer.parseInt(details.getJumlahStok());
//                int qty = Integer.parseInt(total.getText().toString());
//
//                try {
//                    if (stock == 0) {
//                        Toast.makeText(DetailsActivity.this, "No stock", Toast.LENGTH_SHORT).show();
//                    } else {
//                        if (qty == 0) {
//                            Toast.makeText(DetailsActivity.this, "Please enter quantity", Toast.LENGTH_SHORT).show();
//                        } else if (qty > stock) {
//                            Toast.makeText(DetailsActivity.this, "Not enough stock", Toast.LENGTH_SHORT).show();
//                        } else {
//                            storeManager.addToCart(DetailsActivity.this, details.getId(), details.getStoreId(), details.getStoreNama(), details, qty);
//                        }
//                    }
//                } catch (Exception e) {
//                    Toast.makeText(DetailsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }

    public void plus(View view) {
        number = number + 1;
        display(number);
    }

    public void minus(View view) {
        if (number <= 0) {
            number = 0;
        } else {
            number = number - 1;
        }
        display(number);
    }

    private void display(int number) {
        total = findViewById(R.id.details_atc_total);
        total.setText("" + number);
    }

    private void loadTransparentStatusBar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    private void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();

        if (on) {
            params.flags |= bits;
        } else {
            params.flags &= ~bits;
        }

        window.setAttributes(params);
    }
}
