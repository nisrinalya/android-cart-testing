package com.example.sharedpreferences.rest;

import com.example.sharedpreferences.model.AllProduct;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {
    @POST("v2/product/getProductAll")
    Call<AllProduct> getAllProducts();

    @Multipart
    @POST("v2/product/getProductAll")
    Call<AllProduct> getAllProductsPage(@Part("from") RequestBody from, @Part("to") RequestBody to);

    @Multipart
    @POST("v2/product/getProductByID")
    Call<AllProduct> getProductByID(@Part("produkID") RequestBody produkID);
}
