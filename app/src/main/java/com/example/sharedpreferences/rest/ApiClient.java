package com.example.sharedpreferences.rest;

public class ApiClient {
    public static final String BASE_ROOT_URL = "https://api.bakoelasa.com/";

    public static ApiInterface getApiInterface() {
        return RetrofitClient.getClient(BASE_ROOT_URL).create(ApiInterface.class);
    }
}
