package com.example.sharedpreferences.cart.store;

import com.example.sharedpreferences.cart.ProductCart;

import java.util.List;

public class Cart {
    private String storeId;
    private String storeName;
    private List<ProductCart> productCartList;

    public Cart(String storeId, String storeName, List<ProductCart> productCartList) {
        this.storeId = storeId;
        this.storeName = storeName;
        this.productCartList = productCartList;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public List<ProductCart> getProductCartList() {
        return productCartList;
    }

    public void setProductCartList(List<ProductCart> productCartList) {
        this.productCartList = productCartList;
    }
}
