package com.example.sharedpreferences.cart;

import com.example.sharedpreferences.model.product.ProductDetails;

public class ProductCart {
    private String productId;
    private String storeId;
    private String storeName;
    private ProductDetails product;
    private int qty;

    public ProductCart(String productId, String storeId, String storeName, ProductDetails product, int qty) {
        this.productId = productId;
        this.storeId = storeId;
        this.storeName = storeName;
        this.product = product;
        this.qty = qty;
    }

    public ProductCart(String productId, ProductDetails product, int qty) {
        this.productId = productId;
        this.product = product;
        this.qty = qty;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public ProductDetails getProduct() {
        return product;
    }

    public void setProduct(ProductDetails product) {
        this.product = product;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
