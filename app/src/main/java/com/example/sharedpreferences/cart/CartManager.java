package com.example.sharedpreferences.cart;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.sharedpreferences.model.product.ProductDetails;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CartManager {
    public static final String PREF_NAME = "BAKOELASA";
    public static final String KEY_CART_LIST = "cart_list";

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    public CartManager() {
        super();
    }

    public void saveCart(Context context, List<ProductCart> cartList) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();

        Gson gson = new Gson();
        String strCartList = gson.toJson(cartList);

        editor.putString(KEY_CART_LIST, strCartList);

        editor.commit();
    }

    public void addToCart(Context context, String productId, String storeId, String storeName, ProductDetails product, int qty) {
        List<ProductCart> cartList = getCart(context);
        ProductCart cart;

        if (cartList == null || cartList.size() == 0) {
            cartList = new ArrayList<>();

            cart = new ProductCart(productId, storeId, storeName, product, qty);
            cartList.add(cart);
            Toast.makeText(context, "1 - Added to cart", Toast.LENGTH_SHORT).show();
        } else {
            int isEqual = 0;
            for (int i = 0; i < cartList.size(); i++) {
                ProductCart pc = cartList.get(i);
                if (pc.getProductId().equals(productId)) {
                    int currQty = pc.getQty();
                    currQty += qty;

                    cart = new ProductCart(productId, storeId, storeName, product, currQty);
                    cartList.set(cartList.indexOf(pc), cart);
                    Toast.makeText(context, "2 - Added to cart", Toast.LENGTH_SHORT).show();
                    isEqual = 1;
                    break;
                }
            }

            if(isEqual == 0){
                cart = new ProductCart(productId, storeId, storeName, product, qty);
                cartList.add(cart);
                Toast.makeText(context, "3 - Added to cart", Toast.LENGTH_SHORT).show();
            }
        }

        saveCart(context, cartList);
    }

    public void removeFromCart(Context context, List<ProductCart> cartList, ProductCart cart) {
        if (cartList != null) {
            cartList.remove(cartList.indexOf(cart));
            saveCart(context, cartList);

            Toast.makeText(context, cart.getProduct().getNama() + " deleted from cart", Toast.LENGTH_SHORT).show();
        }
    }

    public ArrayList<ProductCart> getCart(Context context) {
        List<ProductCart> cart;
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        if (pref.contains(KEY_CART_LIST)) {
            String strCart = pref.getString(KEY_CART_LIST, null);
            Gson gson = new Gson();
            ProductCart[] cartItems = gson.fromJson(strCart, ProductCart[].class);

            cart = Arrays.asList(cartItems);
            cart = new ArrayList<>(cart);
        } else {
            return null;
        }

        return (ArrayList<ProductCart>) cart;
    }
}
