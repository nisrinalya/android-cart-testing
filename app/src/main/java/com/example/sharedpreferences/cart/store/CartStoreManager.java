package com.example.sharedpreferences.cart.store;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.sharedpreferences.cart.ProductCart;
import com.example.sharedpreferences.model.product.ProductDetails;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CartStoreManager {
    public static final String PREF_NAME = "BAKOELASA";
    public static final String KEY_CART_STORE_LIST = "cart_store_list";

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    public CartStoreManager() {
        super();
    }

    public void saveCart(Context context, List<Cart> cartList) {
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = pref.edit();

        Gson gson = new Gson();
        String strCartList = gson.toJson(cartList);

        editor.putString(KEY_CART_STORE_LIST, strCartList);

        editor.commit();
    }

    public void addToCart(Context context, String productId, String storeId, String storeName, ProductDetails product, int qty) {
        List<Cart> cartList = getCart(context);
        Cart cart;

        if (cartList == null || cartList.size() == 0) {
            cartList = new ArrayList<>();
            List<ProductCart> productCartList = new ArrayList<>();
            ProductCart productCart;

            productCart = new ProductCart(productId, storeId, storeName, product, qty);
            productCartList.add(productCart);

            cart = new Cart(storeId, storeName, productCartList);
            cartList.add(cart);

            Toast.makeText(context, "New Product, create new store list", Toast.LENGTH_SHORT).show();
        } else {
            int isEqual = 0;
            for (int i = 0; i < cartList.size(); i++) {
                Cart c = cartList.get(i);
                List<ProductCart> productCartList = c.getProductCartList();
                ProductCart productCart;

                if (c.getStoreId().equals(storeId)) {
                    int isEquals = 0;
                    for (int j = 0; j < productCartList.size(); j++) {
                        ProductCart pc = productCartList.get(j);

                        if (pc.getProductId().equals(productId)) {
                            int currQty = pc.getQty();
                            currQty += qty;

                            productCart = new ProductCart(productId, storeId, storeName, product, currQty);
                            productCartList.set(productCartList.indexOf(pc), productCart);

                            cart = new Cart(storeId, storeName, productCartList);
                            cartList.set(cartList.indexOf(c), cart);

                            Toast.makeText(context, "Update product, update store", Toast.LENGTH_SHORT).show();
                            isEquals = 1;
                            break;
                        }
                    }

                    if (isEquals == 0) {
                        productCart = new ProductCart(productId, storeId, storeName, product, qty);
                        productCartList.add(productCart);

                        cart = new Cart(storeId, storeName, productCartList);
                        cartList.set(cartList.indexOf(c), cart);

                        Toast.makeText(context, "New product, update store", Toast.LENGTH_SHORT).show();
                    }

                    isEqual = 1;
                    break;
                }
            }

            if (isEqual == 0) {
                List<ProductCart> productCartList = new ArrayList<>();

                ProductCart productCart = new ProductCart(productId, storeId, storeName, product, qty);
                productCartList.add(productCart);

                cart = new Cart(storeId, storeName, productCartList);
                cartList.add(cart);

                Toast.makeText(context, "New product, new store", Toast.LENGTH_SHORT).show();
            }
        }

        saveCart(context, cartList);
    }

    public ArrayList<Cart> getCart(Context context) {
        List<Cart> cartList;
        pref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        if (pref.contains(KEY_CART_STORE_LIST)) {
            String strCartList = pref.getString(KEY_CART_STORE_LIST, null);
            Gson gson = new Gson();
            Cart[] cartItems = gson.fromJson(strCartList, Cart[].class);

            cartList = Arrays.asList(cartItems);
            cartList = new ArrayList<>(cartList);
        } else {
            return null;
        }

        return (ArrayList<Cart>) cartList;
    }
}
