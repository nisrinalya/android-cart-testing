package com.example.sharedpreferences.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sharedpreferences.DetailsActivity;
import com.example.sharedpreferences.R;
import com.example.sharedpreferences.model.product.ProductDetails;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    Context context;
    private List<ProductDetails> productList;

    public MainAdapter(Context context, List<ProductDetails> productList) {
        this.context = context;
        this.productList = productList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ProductDetails currProduct = productList.get(position);
        int intPrice = Integer.parseInt(currProduct.getHarga());

        if (currProduct.getFotoUtama() != null && currProduct.getFotoUtama().length() != 0) {
            Picasso.get().load(currProduct.getFotoUtama()).into(holder.image);
        } else {
            holder.image.setImageResource(R.drawable.bakoelasa_imagedefault_01);
        }

        holder.name.setText(currProduct.getNama());
        holder.price.setText("Rp " + String.format(Locale.getDefault(), "%,d", intPrice));
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent details = new Intent(context, DetailsActivity.class);
                Bundle bundle = new Bundle();

                bundle.putSerializable("prod_list", currProduct);
                details.putExtras(bundle);

                context.startActivity(details);
            }
        });
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name, price;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.item_main_image);

            name = itemView.findViewById(R.id.item_main_name);
            price = itemView.findViewById(R.id.item_main_price);
        }
    }
}
