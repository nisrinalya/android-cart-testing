package com.example.sharedpreferences.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sharedpreferences.CartActivity;
import com.example.sharedpreferences.R;
import com.example.sharedpreferences.cart.CartManager;
import com.example.sharedpreferences.cart.ProductCart;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.ViewHolder> {
    Context context;
    private List<ProductCart> cartList;
    private CartManager cartManager;

    public CartAdapter(Context context, List<ProductCart> cartList, CartManager cartManager) {
        this.context = context;
        this.cartList = cartList;
        this.cartManager = cartManager;
    }

    public CartAdapter(Context context, List<ProductCart> cartList) {
        this.context = context;
        this.cartList = cartList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ProductCart currCart = cartList.get(position);
        int intPrice = Integer.parseInt(currCart.getProduct().getHarga());

        if (currCart.getProduct().getFotoUtama() != null && currCart.getProduct().getFotoUtama().length() != 0) {
            Picasso.get().load(currCart.getProduct().getFotoUtama()).into(holder.image);
        } else {
            holder.image.setImageResource(R.drawable.bakoelasa_imagedefault_01);
        }

        holder.name.setText(currCart.getProduct().getNama());
        holder.price.setText("Rp " + String.format(Locale.getDefault(), "%,d", intPrice));
        holder.total.setText("" + currCart.getQty());

        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cartManager.removeFromCart(context, cartList, currCart);

                CartActivity activity = (CartActivity) context;
                activity.finish();
                activity.overridePendingTransition(0, 0);
                activity.startActivity(activity.getIntent());
                activity.overridePendingTransition(0, 0);
            }
        });
        holder.remove.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image, remove;
        TextView name, price, total;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.item_cart_image);
            remove = itemView.findViewById(R.id.item_cart_remove);

            name = itemView.findViewById(R.id.item_cart_name);
            price = itemView.findViewById(R.id.item_cart_price);
            total = itemView.findViewById(R.id.item_cart_total);
        }
    }
}
