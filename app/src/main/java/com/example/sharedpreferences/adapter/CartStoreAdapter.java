package com.example.sharedpreferences.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.sharedpreferences.R;
import com.example.sharedpreferences.cart.store.Cart;
import com.example.sharedpreferences.cart.store.CartStoreManager;

import java.util.List;

public class CartStoreAdapter extends RecyclerView.Adapter<CartStoreAdapter.ViewHolder> {
    Context context;

    private List<Cart> cartList;
    private CartStoreManager storeManager;

    public CartStoreAdapter(Context context, List<Cart> cartList, CartStoreManager storeManager) {
        this.context = context;
        this.cartList = cartList;
        this.storeManager = storeManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart_store, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Cart currStore = cartList.get(position);

        holder.store.setText(currStore.getStoreId());

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        CartAdapter adapter = new CartAdapter(context, currStore.getProductCartList());

        holder.product.setLayoutManager(layoutManager);
        holder.product.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return cartList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView store;
        RecyclerView product;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            store = itemView.findViewById(R.id.item_cart_store_name);
            product = itemView.findViewById(R.id.item_cart_store_content);
        }
    }
}
