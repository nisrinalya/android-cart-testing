package com.example.sharedpreferences;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.example.sharedpreferences.adapter.CartStoreAdapter;
import com.example.sharedpreferences.cart.store.Cart;
import com.example.sharedpreferences.cart.store.CartStoreManager;

import java.util.List;

public class CartStoreActivity extends AppCompatActivity {
    private CartStoreManager storeManager;
    private List<Cart> cartList;
    private CartStoreAdapter storeAdapter;

    RecyclerView recycler;
    View viewSubtotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart_store);

        loadWhiteStatusBar();

        storeManager = new CartStoreManager();
        cartList = storeManager.getCart(CartStoreActivity.this);

        recycler = findViewById(R.id.cart_store_content);
        viewSubtotal = findViewById(R.id.cart_store_subtotal_container);
        viewSubtotal.setVisibility(View.GONE);

        if (cartList != null && cartList.size() != 0) {
            RecyclerView.LayoutManager layoutManager =  new LinearLayoutManager(CartStoreActivity.this);
            storeAdapter = new CartStoreAdapter(CartStoreActivity.this, cartList, storeManager);

            recycler.setLayoutManager(layoutManager);
            recycler.setAdapter(storeAdapter);
        } else {
            recycler.setVisibility(View.GONE);
        }
    }

    private void loadWhiteStatusBar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
        if (Build.VERSION.SDK_INT >= 23) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().setStatusBarColor(Color.WHITE);
        }
    }

    private void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams params = window.getAttributes();

        if (on) {
            params.flags |= bits;
        } else {
            params.flags &= ~bits;
        }

        window.setAttributes(params);
    }
}
