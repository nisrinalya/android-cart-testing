package com.example.sharedpreferences.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductCategory1 implements Serializable {
    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("nama")
    @Expose
    private Object nama;

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getNama() {
        return nama;
    }

    public void setNama(Object nama) {
        this.nama = nama;
    }
}
