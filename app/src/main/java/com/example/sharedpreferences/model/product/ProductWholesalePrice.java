package com.example.sharedpreferences.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductWholesalePrice implements Serializable {
    @SerializedName("minimum")
    @Expose
    private String minimum;
    @SerializedName("harga")
    @Expose
    private String harga;

    public ProductWholesalePrice(String minimum, String harga) {
        this.minimum = minimum;
        this.harga = harga;
    }

    public String getMinimum() {
        return minimum;
    }

    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
