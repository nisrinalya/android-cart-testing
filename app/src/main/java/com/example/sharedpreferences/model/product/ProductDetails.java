package com.example.sharedpreferences.model.product;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ProductDetails implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("storeId")
    @Expose
    private String storeId;
    @SerializedName("storeNama")
    @Expose
    private String storeNama;
    @SerializedName("category_parent")
    @Expose
    private ProductCategoryParent categoryParent;
    @SerializedName("category1")
    @Expose
    private ProductCategory1 category1;
    @SerializedName("category2")
    @Expose
    private ProductCategory2 category2;
    @SerializedName("category3")
    @Expose
    private ProductCategory3 category3;
    @SerializedName("etalase")
    @Expose
    private ProductDisplay etalase;
    @SerializedName("nama")
    @Expose
    private String nama;
    @SerializedName("harga")
    @Expose
    private String harga;
    @SerializedName("harga_potongan")
    @Expose
    private String hargaPotongan;
    @SerializedName("berat")
    @Expose
    private String berat;
    @SerializedName("kondisi")
    @Expose
    private ProductCondition kondisi;
    @SerializedName("status_stok")
    @Expose
    private String statusStok;
    @SerializedName("jumlah_stok")
    @Expose
    private String jumlahStok;
    @SerializedName("is_preorder")
    @Expose
    private String isPreorder;
    @SerializedName("url_video")
    @Expose
    private String urlVideo;
    @SerializedName("deskripsi")
    @Expose
    private String deskripsi;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("total_terjual")
    @Expose
    private String totalTerjual;
    @SerializedName("foto_utama")
    @Expose
    private String fotoUtama;
    @SerializedName("foto_depan")
    @Expose
    private String fotoDepan;
    @SerializedName("foto_samping")
    @Expose
    private String fotoSamping;
    @SerializedName("foto_atas")
    @Expose
    private String fotoAtas;
    @SerializedName("foto_detail")
    @Expose
    private String fotoDetail;
    @SerializedName("harga_grosir")
    @Expose
    private List<ProductWholesalePrice> hargaGrosir = null;
    @SerializedName("customerFavorite")
    @Expose
    private String customerFavorite;

    public ProductDetails(String id, String storeId, String storeNama, ProductCategoryParent categoryParent, ProductCategory1 category1, ProductCategory2 category2, ProductCategory3 category3, ProductDisplay etalase, String nama, String harga, String hargaPotongan, String berat, ProductCondition kondisi, String statusStok, String jumlahStok, String isPreorder, String urlVideo, String deskripsi, String status, String totalTerjual, String fotoUtama, String fotoDepan, String fotoSamping, String fotoAtas, String fotoDetail, List<ProductWholesalePrice> hargaGrosir, String customerFavorite) {
        this.id = id;
        this.storeId = storeId;
        this.storeNama = storeNama;
        this.categoryParent = categoryParent;
        this.category1 = category1;
        this.category2 = category2;
        this.category3 = category3;
        this.etalase = etalase;
        this.nama = nama;
        this.harga = harga;
        this.hargaPotongan = hargaPotongan;
        this.berat = berat;
        this.kondisi = kondisi;
        this.statusStok = statusStok;
        this.jumlahStok = jumlahStok;
        this.isPreorder = isPreorder;
        this.urlVideo = urlVideo;
        this.deskripsi = deskripsi;
        this.status = status;
        this.totalTerjual = totalTerjual;
        this.fotoUtama = fotoUtama;
        this.fotoDepan = fotoDepan;
        this.fotoSamping = fotoSamping;
        this.fotoAtas = fotoAtas;
        this.fotoDetail = fotoDetail;
        this.hargaGrosir = hargaGrosir;
        this.customerFavorite = customerFavorite;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreNama() {
        return storeNama;
    }

    public void setStoreNama(String storeNama) {
        this.storeNama = storeNama;
    }

    public ProductCategoryParent getCategoryParent() {
        return categoryParent;
    }

    public void setCategoryParent(ProductCategoryParent categoryParent) {
        this.categoryParent = categoryParent;
    }

    public ProductCategory1 getCategory1() {
        return category1;
    }

    public void setCategory1(ProductCategory1 category1) {
        this.category1 = category1;
    }

    public ProductCategory2 getCategory2() {
        return category2;
    }

    public void setCategory2(ProductCategory2 category2) {
        this.category2 = category2;
    }

    public ProductCategory3 getCategory3() {
        return category3;
    }

    public void setCategory3(ProductCategory3 category3) {
        this.category3 = category3;
    }

    public ProductDisplay getEtalase() {
        return etalase;
    }

    public void setEtalase(ProductDisplay etalase) {
        this.etalase = etalase;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getHargaPotongan() {
        return hargaPotongan;
    }

    public void setHargaPotongan(String hargaPotongan) {
        this.hargaPotongan = hargaPotongan;
    }

    public String getBerat() {
        return berat;
    }

    public void setBerat(String berat) {
        this.berat = berat;
    }

    public ProductCondition getKondisi() {
        return kondisi;
    }

    public void setKondisi(ProductCondition kondisi) {
        this.kondisi = kondisi;
    }

    public String getStatusStok() {
        return statusStok;
    }

    public void setStatusStok(String statusStok) {
        this.statusStok = statusStok;
    }

    public String getJumlahStok() {
        return jumlahStok;
    }

    public void setJumlahStok(String jumlahStok) {
        this.jumlahStok = jumlahStok;
    }

    public String getIsPreorder() {
        return isPreorder;
    }

    public void setIsPreorder(String isPreorder) {
        this.isPreorder = isPreorder;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public void setUrlVideo(String urlVideo) {
        this.urlVideo = urlVideo;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotalTerjual() {
        return totalTerjual;
    }

    public void setTotalTerjual(String totalTerjual) {
        this.totalTerjual = totalTerjual;
    }

    public String getFotoUtama() {
        return fotoUtama;
    }

    public void setFotoUtama(String fotoUtama) {
        this.fotoUtama = fotoUtama;
    }

    public String getFotoDepan() {
        return fotoDepan;
    }

    public void setFotoDepan(String fotoDepan) {
        this.fotoDepan = fotoDepan;
    }

    public String getFotoSamping() {
        return fotoSamping;
    }

    public void setFotoSamping(String fotoSamping) {
        this.fotoSamping = fotoSamping;
    }

    public String getFotoAtas() {
        return fotoAtas;
    }

    public void setFotoAtas(String fotoAtas) {
        this.fotoAtas = fotoAtas;
    }

    public String getFotoDetail() {
        return fotoDetail;
    }

    public void setFotoDetail(String fotoDetail) {
        this.fotoDetail = fotoDetail;
    }

    public List<ProductWholesalePrice> getHargaGrosir() {
        return hargaGrosir;
    }

    public void setHargaGrosir(List<ProductWholesalePrice> hargaGrosir) {
        this.hargaGrosir = hargaGrosir;
    }

    public String getCustomerFavorite() {
        return customerFavorite;
    }

    public void setCustomerFavorite(String customerFavorite) {
        this.customerFavorite = customerFavorite;
    }
}
